import React, { Fragment } from 'react';
import DataTable from './components/data/DataTable';
import './App.css';

function App() {
  return (
    <Fragment>
      <div className='App '>
        <div className='container'>
          <DataTable />
        </div>
      </div>
    </Fragment>
  );
}

export default App;

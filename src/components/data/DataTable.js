import React, { useState, useRef } from 'react';
import XLSX from 'xlsx';
import DataItem from './DataItem';
import { Table } from 'semantic-ui-react'

const DataTable = () => {
  const [rows, setRows] = useState([]);
  const [cols, setCols] = useState([]);
  const fileRef = useRef();

  const fileHandler = (event) => {
    let fileObj = event.target.files[0];

    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array' });
      /* Get first worksheet */

      /* Get sheet named DATA */
      const ws = wb.Sheets['DATA'];
      /* if not there return */
      if (!ws) {
        alert('File does not contain DATA sheet!');
        fileRef.current.value = '';
        return;
      }
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, { header: 1 });

      /* Print dat data */
      let cols = make_cols(ws['!ref']);
      setCols(cols);
      setRows(data);
    };
    if (rABS) reader.readAsBinaryString(fileObj);
    else reader.readAsArrayBuffer(fileObj);
  };

  const clearData = () => {
    fileRef.current.value = '';
    setCols([]);
    setRows([]);
  };

  return (
    <div className='container' style={{ padding: '10px', textAlign: 'left' }}>
      <div className='ui input' style={{ marginBottom: '10px' }}>
        <input
          type='file'
          ref={fileRef}
          accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
          onChange={fileHandler}
          style={{ padding: '10px' }}
        />
        {rows.length > 0 && (
          <button
            className='ui button'
            onClick={clearData}
            style={{ marginLeft: '10px' }}
          >
            Clear
          </button>
        )}
      </div>

      {rows.length > 0 && (
        <div className='scrollTable' style={{ maxHeight: '500px' }}>
          <Table striped selectable textAlign='center'>
            <thead>
              <tr>
                {rows[0].map((e, i) => {
                  return <th key={i}>{e}</th>
                })}

              </tr>
            </thead>
            <tbody >
              {rows.slice(1).map((e, i) => {
                return (
                  <tr key={i} >
                    <DataItem row={e} size={cols.length} />
                  </tr>
                );
              })}
            </tbody>
          </Table>

        </div>
      )
      }
    </div >
  );
};

// create array with columns names
function make_cols(refstr /*:string*/) {
  var o = [];
  var range = XLSX.utils.decode_range(refstr);
  for (var i = 0; i <= range.e.c; ++i) {
    o.push({ name: XLSX.utils.encode_col(i), key: i });
  }

  return o;
}
export default DataTable;

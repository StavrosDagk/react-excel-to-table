import React from 'react';

const DataItem = ({ row, size }) => {
  var foo = new Array(size).fill('');
  if (row.length > 0) foo.splice(0, row.length, ...row);
  return foo.map((element, i) => {
    return <td key={i}>{element}</td>;
  });
};

export default DataItem;
